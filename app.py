# -*- coding: utf-8 -*-

import boto3
import datetime

def dump_stats(event=None, lambda_context=None):
    dump_stats_handler()

def dump_stats_handler(today=datetime.date.today()):
    yesterday = today - datetime.timedelta(days=1)
    ce = boto3.client("ce")
    response = ce.get_cost_and_usage(
        TimePeriod={"Start": f"{yesterday}", "End": f"{today}"},
        Metrics=["BLENDED_COST"],
        Granularity="MONTHLY",
    )
    bill = float(response["ResultsByTime"][0]["Total"]["BlendedCost"]["Amount"])
    print(bill)

if __name__ == "__main__":
    dump_stats_handler()
